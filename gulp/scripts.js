/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var gulp = require('gulp');
//var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify-es').default;

var vinylToStream = require('vinyl-to-stream');
var StreamConcat = require('stream-concat');

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});

/**
 * Variables
 */
var srcDir = './app/scripts';
var entryFile = 'app.js';
var targetFile = 'app.js';

var tmpDir = './.tmp/scripts';
var dstDir = './dist/scripts';


/**
 * @param obfuscate
 * @param watch
 * @returns {*}
 */
function buildBundle(input, obfuscate, watch, dest) {
    var bundler = browserify({
        entries: input,
        debug: false,
        cache: {},
        packageCache: {},
        extensions: ['.js', '.json', '.jsx'],
        basedir: srcDir,
        //paths: ["."],
        fullPaths: false
    });

    bundler = bundler.transform('require-globify', {global: true});
    bundler = bundler.transform('yamlify', {global: true});
    bundler = bundler.transform('stringify', {
        global: true,
        minify: true
    });

    // babelify ?
    //bundler = bundler.transform('babelify', {presets: ['es2015', 'react']});

    function bundle(b) {
        var startMs = Date.now();
        var db = b.bundle()
            .on('error', function(error) {
                $.util.log($.util.colors.red(error.message));
            })
            .pipe(source(targetFile));

        if (obfuscate) {
            db
                .pipe($.streamify($.ngAnnotate()))
                .pipe($.streamify(uglify()))
                .on('error', function(error) {
                    $.util.log($.util.colors.red(error.toString()));
                });
        }

        // optional, remove if you dont want sourcemaps
        //db.pipe(sourcemaps.init({loadMaps: true}));       // loads map from browserify file
        //db.pipe(sourcemaps.write('./'));                  // Add transformation tasks to the pipeline here.

        db.pipe(gulp.dest(dest));
        console.log('Updated browserified file in', (Date.now() - startMs) + 'ms');
        return db;
    }

    if (watch) {
        bundler = watchify(bundler)
            .on('update', function() {
                bundle(bundler);
            });
    }

    return bundle(bundler);
}

/**
 * @type {{build: module.exports.build, watch: module.exports.watch}}
 */
module.exports = {
    build: function() {
        return buildBundle(entryFile, true, false, dstDir);
    },
    watch: function() {
        buildBundle(entryFile, false, true, tmpDir);
    }
};