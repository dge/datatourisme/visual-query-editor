/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var gulp = require('gulp');
var del = require('del');

var gulp_scripts = require('./gulp/scripts');
var gulp_styles = require('./gulp/styles');
var gulp_html = require('./gulp/html');
var gulp_serve = require('./gulp/serve');

/**
 * clean
 * @todo : https://github.com/gulpjs/gulp/blob/master/docs/recipes/running-tasks-in-series.md
 * @see https://github.com/gulpjs/gulp/issues/686
 **/
gulp.task('clean', function () {
    return del.sync(['.tmp', 'dist']);
});

// watches
gulp.task('watch:scripts', gulp_scripts.watch);
gulp.task('watch:styles', gulp_styles.watch);
gulp.task('watch', ['watch:scripts', 'watch:styles']);

// builds
gulp.task('build:scripts', gulp_scripts.build);
gulp.task('build:styles', gulp_styles.build);
gulp.task('build:html', gulp_html.build);
gulp.task('build', ['build:scripts', 'build:styles', 'build:html']);

// serve
gulp.task('serve', ['watch'], gulp_serve.dev);
gulp.task('serve:dist', ['watch'], gulp_serve.dist);

// default
gulp.task('default', ['serve']);
