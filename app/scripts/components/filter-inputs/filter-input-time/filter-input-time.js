/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('filterInputTime', {
        controller: controller,
        template: require("./filter-input-time.html"),
        require: {
            ngModelCtrl: 'ngModel'
        }
    });

/**
 * @ngInject
 */
function controller(ngDialog, $http, $timeout) {
    this.operator = "_eq";
    this.value = null;
    this.onChange = function() {
        if(this.operator && this.value) {
            var obj = {};
            obj[this.operator] = this.value;
            this.ngModelCtrl.$setViewValue(obj);
        } else {
            this.ngModelCtrl.$setViewValue(null);
        }
    }
}
