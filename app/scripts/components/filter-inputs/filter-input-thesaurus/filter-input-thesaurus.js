/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

require('angular')
    .module('visual-query-editor')
    .component('filterInputThesaurus', {
        controller: controller,
        template: require("./filter-input-thesaurus.html"),
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            ngModel: '=',
            property: '<'
        }
    });

/**
 * @ngInject
 */
function controller(ontology, $translate) {
    var $ctrl = this;

    this.options = [];
    this.config = {
        optgroupField: 'group',
        optgroupValueField: 'uri',
        optgroupLabelField: 'label',
        searchField: ['label'],
        valueField: 'uri',
        labelField: 'label',
        placeholder: $translate.instant('placeholder.thesaurus'),
        optgroups: []
    };

    this.$onChanges = function () {

        var ranges = this.property.ranges;
        var options = [];
        var optgroups = {};

        for(var i=0; i<ranges.length; i++) {
            var individuals = ranges[i].getIndividuals(false);
            for(var j=0; j<individuals.length; j++) {
                var individual = individuals[j];
                var group = individual.getType();
                var label = individual.getPreferedLabel();

                // specific : add insee code to cities
                if(ranges[i].hasUri(ontology.NS + "City")) {
                    label += " (" + individual.getPropertyValues(ontology.NS + "insee")[0] + ")";
                }

                options.push({
                    uri: individual.getUri(),
                    label: label,
                    group: group.getUri()
                });

                if(!optgroups[group.getUri()]) {
                    optgroups[group.getUri()] = {
                        uri: group.getUri(),
                        label: group.getPreferedLabel()
                    };
                }
            }
        }

        var optgroupsArr = [];
        for (var key in optgroups) {
            optgroupsArr.push(optgroups[key]);
        }
        this.config.optgroups = optgroupsArr;

        options = options.sort(sortByLabel);
        this.options = options;
    };

}

/**
 * @param a
 * @param b
 * @returns {number}
 */
function sortByLabel(a, b) {
    if(a.label < b.label) return -1;
    else if(b.label < a.label) return 1;
    return 0;
}
