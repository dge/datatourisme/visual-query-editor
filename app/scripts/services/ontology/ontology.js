/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var Ontology = require('ontology-js');

require('angular')
    .module('visual-query-editor')
    .provider('ontology', ontologyProvider);

/**
 * @ngInject
 */
function ontologyProvider() {
    var ontology = new Ontology();

    this.setOntology = function(value) {
        ontology = value;
        ontology.NS = "https://www.datatourisme.gouv.fr/ontology/core#";
    };

    /**
     * @ngInject
     */
    this.$get = function() {
        return ontology;
    };
}



