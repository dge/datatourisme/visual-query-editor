/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'text';

Filter.definition = {
    label: "Recherche par mots-clé",
    helper: "Mots-clé",
    comment: "Permet de faire une recherche par mots-clé"
};

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res, property) {
    return (property.getType() == 'data');
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<input type="text" class="form-control input-sm" ng-model="$ctrl.ngModel" ng-model-options="{ allowInvalid: false, debounce: 500 }" required empty-to-null>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, {_text: value });
};


module.exports = Filter;