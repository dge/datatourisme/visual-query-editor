/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var AbstractFilter = require('./abstract-filter.js');

var Filter = function (property) {
    AbstractFilter.call(this, property); // call parent constructor
};
Filter.prototype = Object.create(AbstractFilter.prototype);
Filter.prototype.constructor = Filter;
Filter.type = 'number';

/**
 * support
 * @returns {boolean}
 */
Filter.supports = function(res) {
    return res.hasUri("http://www.w3.org/2001/XMLSchema#int") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#integer") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#decimal") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#byte") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#long") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#nonNegativeInteger") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#positiveInteger") ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#short")/* ||
        res.hasUri("http://www.w3.org/2001/XMLSchema#time")*/;
};

/**
 * getTemplate
 * @returns {string}
 */
Filter.prototype.template = function() {
    return '<filter-input-number ng-model="$ctrl.ngModel"></filter-input-number>';
};

/**
 * graphql
 */
Filter.prototype.graphql = function(path, value) {
    return this.graphqlPath(path, value);
};


module.exports = Filter;