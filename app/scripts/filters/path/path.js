/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

// service definition
angular
    .module('visual-query-editor')
    .filter('path', path);

/**
 * @ngInject
 */
function path($translate) {
    return function(obj, path) {
        var parts = path.split(".");
        var next = parts.shift();
        main: while(next) {
            if(!obj[next]) {
                return null;
            }
            if(obj[next] instanceof Array) {

                if(obj[next].length == 0) {
                    return null;
                }

                // ------
                if(obj[next][0]['lang']) {
                    var fallback = null;
                    for(var i=0; i<obj[next].length; i++) {
                        if(obj[next][i]['lang'] == $translate.proposedLanguage()) {
                            obj = obj[next][i];
                            next = parts.shift();
                            continue main;
                        } else if(obj[next][i]['lang'] == 'fr') {
                            fallback = obj[next][i];
                        }
                    }
                    if(fallback) {
                        obj = fallback;
                        next = parts.shift();
                        continue;
                    }
                }
                // ----

                obj = obj[next][0];
            } else {
                obj = obj[next];
            }

            next = parts.shift();
        }

        return obj;
    }
}